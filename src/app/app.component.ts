import { Component } from '@angular/core';
import { Post } from './post';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
}
)
export class AppComponent {
  title = 'Posts';
  list_posts: Post[]= [
    new Post("Mon Premier Post","blabla 1er et mlkmker mlk emlke melk rem lke elmkr kmelk",0),
    new Post("Mon deuxième Post","blabla 2er et mlkmker mlk emlke melk rem lke elmkr kmelk",0),
    new Post("Encore un Post","blabla dernier et mlkmker mlk emlke melk rem lke elmkr kmelk",0)
  ]
  // list_posts: Post[]= [
  //   {'Mon Premier Post',content:'blabla 1er et mlkmker mlk emlke melk rem lke elmkr kmelk ',loveIts:0, created_at:},
  //   {title:'Mon deuxième Post',content:'blabla 2er et mlkmker mlk emlke melk rem lke elmkr kmelk ',loveIts:0, created_at:new Date()},
  //   {title:'Encore un Post',content:'blabla dernier et mlkmker mlk emlke melk rem lke elmkr kmelk ',loveIts:0, created_at:new Date()}
  // ]
}






