import { Component, OnInit,Input } from '@angular/core';
import { Post } from '../post';
import { CloneVisitor } from '@angular/compiler/src/i18n/i18n_ast';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.scss']
})
export class PostListItemComponent implements OnInit {
  color;
  @Input() child_post_item: Post;
   constructor() { }
  ngOnInit() {
  }
loveIt(){
this.child_post_item.loveIts++;
}
dontLoveIt(){
 this.child_post_item.loveIts--;
}

get_color()
{
if (this.child_post_item.loveIts>0){
this.color="#B5EAAA"; 
}
else if (this.child_post_item.loveIts==0)
{
this.color="white";
}
else if (this.child_post_item.loveIts<0)
{
this.color="#ECC5C0";
}
return this.color;
}

}
